'use strict';

/**
 *  dibs-planning-date controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::dibs-planning-date.dibs-planning-date');
