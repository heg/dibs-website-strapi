'use strict';

/**
 * dibs-planning-date router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::dibs-planning-date.dibs-planning-date');
