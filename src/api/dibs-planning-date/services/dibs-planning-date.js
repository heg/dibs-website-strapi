'use strict';

/**
 * dibs-planning-date service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::dibs-planning-date.dibs-planning-date');
