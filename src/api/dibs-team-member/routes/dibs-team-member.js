'use strict';

/**
 * dibs-team-member router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::dibs-team-member.dibs-team-member');
