'use strict';

/**
 * dibs-team-member service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::dibs-team-member.dibs-team-member');
