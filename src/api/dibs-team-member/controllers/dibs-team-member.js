'use strict';

/**
 *  dibs-team-member controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::dibs-team-member.dibs-team-member');
