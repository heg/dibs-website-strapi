'use strict';

/**
 * special-section router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::special-section.special-section');
