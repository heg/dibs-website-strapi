'use strict';

/**
 *  special-section controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::special-section.special-section');
