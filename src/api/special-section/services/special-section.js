'use strict';

/**
 * special-section service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::special-section.special-section');
