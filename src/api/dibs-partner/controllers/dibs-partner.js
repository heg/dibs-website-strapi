'use strict';

/**
 *  dibs-partner controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::dibs-partner.dibs-partner');
