'use strict';

/**
 * dibs-partner router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::dibs-partner.dibs-partner');
