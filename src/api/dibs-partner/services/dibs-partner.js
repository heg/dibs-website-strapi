'use strict';

/**
 * dibs-partner service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::dibs-partner.dibs-partner');
