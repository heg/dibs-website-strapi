'use strict';

/**
 * dibs-publication router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::dibs-publication.dibs-publication');
