'use strict';

/**
 * dibs-publication service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::dibs-publication.dibs-publication');
