'use strict';

/**
 *  dibs-publication controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::dibs-publication.dibs-publication');
