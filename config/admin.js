module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'), // only used along with `strapi develop --watch-admin` command
  port: env('PORT', '1337'), // only used along with `strapi develop --watch-admin` command
  auth: {
    secret: env('ADMIN_JWT_SECRET', '26934856a801ff3a019ed51eb2459d1c'),
  },
   url: '/admin',
});
